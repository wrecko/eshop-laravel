<?php 
 
namespace App\Lib;

class Cart
{

	private static 	$_products = [];

	public static function getCart()
	{
		$cart = session()->get('cart');
		if(!empty($cart))
		{	
			self::$_products = $cart['products'];
		}
				
		return $cart;
	}

	public static function getCartProducts()
	{
		if(!isset(self::$_products))
			self::getCart();
		return self::$_products;
	}

	public static function getProductCount()
	{
		return self::calculate_products()[0];
	}

	private static function saveCart()
	{
		$calculations = self::calculate_products();
		$cart = 
		[
			'products'			=> self::$_products,
			'product_count'		=> $calculations[0],
			'total_price'		=> $calculations[1],
		];
		session()->put('cart', $cart);
		return $cart;
	}

	public static function updateCart($products/*$product, $request*/)
	{
		if(!isset(self::$_products))
			self::getCart();

		self::$_products = $products;
		return self::saveCart();

	}

	public static function clearCart()
	{
		session()->forget('cart');
	}

	private static function calculate_products()
	{
		if(!isset(self::$_products))
			self::getCart();		

		$count = 0;
		$total_price = 0;
		foreach (self::$_products as $id => $prod) {
		 	foreach ($prod['options'] as $option) {
		 		$count += $option['quantity'];
		 		$total_price += $prod['price']*$option['quantity'];
		 	}
		 	
		}
		return [$count, round($total_price, 2)];
	}

}
?>