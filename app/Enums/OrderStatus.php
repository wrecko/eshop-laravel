<?php

	namespace App\Enums;
	
	use App\Enums\BaseEnum;

	final class OrderStatus extends BaseEnum
	{
	    const NEW = 1;
	    const ACCEPTED = 2;
	    const EXPEDITED = 3;
	    const CLOSED = 4;
	    const INVALID = 5;   
	}

?>