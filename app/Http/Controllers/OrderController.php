<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;

class OrderController extends Controller
{
    public function index()
    {
    	return view('admin.order.index', [
    		'orders'	=> Order::all()
    	]);
    }

    public function detail($id)
    {
    	// var_dump($id, Order::findorfail($id)->with('order_products')->get());
    	return view('admin.order.detail', [
    		'order'	=> Order::findorfail($id)
    	]);
    }

    public function change_status($id, $status)
    {
        // var_dump($id, $status);

        if(Order::where('id', $id)->update(['order_status'=> $status]))
        {
            return redirect()->back();
        }
        
    }
}
