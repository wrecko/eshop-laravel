<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductCategory;

class ProductCategoryController extends Controller
{
    public function Index()
    {    	
    	return view('admin.category.index', ['categories' => ProductCategory::all()]);
    }

    public function Create(Request $request)
    {
    	$model = new ProductCategory($request->all());
    	if($model->parentcategoryid == -1)
    		$model->parentcategoryid = null;

    	$model->save();
    	return redirect()->back();
    }

    public function Update(Request $request)
    {
    	$model = ProductCategory::find($request->id);
    	if($model != null)
    	{
    		var_dump($request->all());
    		$model->name = $request->name;
    		if($model->parentcategoryid == -1)
    			$model->parentcategoryid = null;
    		else
    			$model->parentcategoryid = $request->parentcategoryid;
    		$model->save();
    	}
    	
    	 return redirect()->back();
    }

    public function Delete($id)
    {

    	if($id != null && is_numeric($id))
    	{
    		$model = ProductCategory::find($id);
    		if($model != null)
    		{
    			$model->delete();
    			return redirect()->back();
			}
    	}

    	return redirect()->back()->withErrors(['msg', 'The Message']);
    }
}
