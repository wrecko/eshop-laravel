<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use App\Models\OptionType;
use App\Models\Option;
use App\Models\OptionRelation;

class ProductOptionController extends Controller
{
    public function ProductOption()
    {    
    	return view('admin.ProductOption.index', [
    		'optionTypes'=> OptionType::all(), 
    		'options' => Option::all(),
    		'relations' => OptionRelation::all()
    	]);
    }

    public function OptionCreate(Request $request)
    {    	
    	
    	$data = $request->all();
		$model = new Option($data);  
		$model->save();
		return redirect()->back();
    }

    public function OptionUpdate(Request $request)
    {    	
    	$model = Option::find($request->id);
    	if($model != null && $model->exists)
    	{
    		$model->name = $request->name;    	
    		$model->option_type_id = $request->optionTypeId; 
    		$model->save();	
    		return redirect()->back();
    	}

    	 return redirect()->back()->withErrors(['msg', 'The Message']);    	
    }

    public function OptionDelete($id)
    {    	
    	if($id != null && is_numeric($id))
   		{
   			Option::destroy($id);
   		}

   		return redirect()->back();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    public function OptionTypeCreate(Request $request)
    {
    	if(true)
    	{
    		
    		$data = $request->all();
    		$model = new OptionType($data);    		
    		$model->save();
    		return redirect()->back();
    	}    	
    }
    public function OptionTypeUpdate(Request $request)
    {  
    	$model = OptionType::find($request->id);
    	if($model != null && $model->exists)
    	{
    		$model->name = $request->name;    	
    		$model->save();	
    		return redirect()->back();
    	}

    	 return redirect()->back()->withErrors(['msg', 'The Message']);    	
    }

    public function OptionTypeDelete($id)
    {  
   		
   		if($id != null && is_numeric($id))
   		{
   			OptionType::destroy($id);
   		}

   		return redirect()->back();
    }

    /////////////////////////////////////////////////////////////////////////////

    public function OptionRelationCreate(Request $request)
    {
    	if(true)
    	{
    		$data = $request->all();
    		$model = new OptionRelation($data);  

    		$model->save();
    		return redirect()->back();
    	}    	
    }
    public function OptionRelationUpdate(Request $request)
    {  
    	$model = OptionRelation::find($request->id);
    	if($model != null && $model->exists)
    	{
    		$model->name = $request->name;    	
    		$model->save();	
    		return redirect()->back();
    	}

    	 return redirect()->back()->withErrors(['msg', 'The Message']);    	
    }

    public function OptionRelationDelete($id)
    {  
   		
   		if($id != null && is_numeric($id))
   		{
   			OptionRelation::destroy($id);
   		}

   		return redirect()->back();
    }
}
