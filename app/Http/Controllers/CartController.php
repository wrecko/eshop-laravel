<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Option;
use App\Models\ProductQuantity;
use App\Lib\Cart;

class CartController extends Controller
{
    public function cart()
    {    	    	
    	$cart = Cart::getCart();
    	return response()->json(['cart' => view('public._partials.cart', ['cart' => $cart['products'], 'price_total' => $cart['total_price']])->render()]);
    }

    public function add_to_cart(Request $request)
    {
		// TODO: pridaj fotku, ktora bude rovnaka ako option
		// TODO: cart ako objekt, nie pole | automaticke premenne (celkova cena atd.)

    	$cart = Cart::getCart();   
        if(!empty($cart))
            $cart = $cart['products'];

    	$product = Product::findOrFail($request->id);
        $quantity = ProductQuantity::findOrFail($request->quantity_id);
        if($quantity->quantity == 0)
            return response('error', 500);    	


    	if(isset($cart[$request->id]))
    	{  		
    		
			if(isset($cart[$request->id]['options'][$request->quantity_id]))
    		{
    			$cart[$request->id]['options'][$request->quantity_id]['quantity']++;                
    		}
    		else
    		{
    			$cart[$request->id]['options'][$request->quantity_id] = 
	    		[
	    			"parent_id"		=> $request->parent_id,
            		"parent_name"	=> $request->parent_id == null? "": Option::find($request->parent_id)->name,
            		"option_id"		=> $request->option_id,
            		"child_name"	=> $request->option_id == null? "": Option::find($request->option_id)->name,
            		"quantity"		=> 1
	    		];
			}

            $cart[$request->id]['prod_price_total'] += $product->price_vat;
            // return response()->json(['cart' => $cart]);
            // session()->put('cart', $cart);
            Cart::updateCart($cart);
            


            return response('success', 200);
    	}

    	$is_about_to_be_added = 
    	[
            "name" 		       => $product->name,
            "price" 	       => $product->price_vat,
            "photo" 	        => $product->images->first()->path,
            "prod_price_total"  => $product->price_vat,
            "options"	=> [
            	$request->quantity_id => 
	            	[
	            		"parent_id"		=> $request->parent_id,
	            		"parent_name"	=> $request->parent_id == null? "": Option::find($request->parent_id)->name,
	            		"option_id"		=> $request->option_id,
	            		"child_name"	=> $request->option_id == null? "": Option::find($request->option_id)->name,
	            		"quantity"		=> 1
	            	]
            ]
        ];
    	
        $cart[$product->id] = $is_about_to_be_added;
        
        // session()->put('cart', $cart);
        Cart::updateCart($cart);
        return response('asdad', 200);
    }

    public function remove_from_cart(Request $request)
    {
		$cart = Cart::getCart();   
        if(!empty($cart))
            $cart = $cart['products'];

		$quantity_id = $request->quantity_id;
    	$product_id = $request->product_id;

    	if(count($cart[$product_id]['options']) == 1)
    		unset($cart[$product_id]);
    	else
    		unset($cart[$product_id]['options'][$quantity_id]);
    	$updatedCart = Cart::updateCart($cart);

    	return response()->json([
    		'price_total' 	=> $updatedCart['total_price']
    	]);
    }

    public function adjust_cart_product_count (Request $request)
    {
    	$method = $request->method;
    	$quantity_id = $request->quantity_id;
    	$product_id = $request->product_id;
    	$cart = Cart::getCart();   
        if(!empty($cart))
            $cart = $cart['products'];

        $quantity = ProductQuantity::findOrFail($quantity_id);        

    	if(
            $method == 1 && 
            ($quantity->quantity - $cart[$product_id]['options'][$quantity_id]['quantity']) > 0
        )
        {
    		$cart[$product_id]['options'][$quantity_id]['quantity']++;
            $cart[$product_id]['prod_price_total'] += $cart[$product_id]['price'];
        }

    	else if($method == 0 && $cart[$product_id]['options'][$quantity_id]['quantity'] > 0)
        {
    		$cart[$product_id]['options'][$quantity_id]['quantity']--;
            $cart[$product_id]['prod_price_total'] -= $cart[$product_id]['price'];
        }
        


    	$count = $cart[$product_id]['options'][$quantity_id]['quantity'];        

    	$updatedCart = Cart::updateCart($cart);
    	return response()->json([
    		'count' 		=> $count, 
    		'product_total'	=> round($updatedCart['products'][$product_id]['prod_price_total'], 2),
    		'price_total' 	=> $updatedCart['total_price'],
            'remove_plus'   => $quantity->quantity - $cart[$product_id]['options'][$quantity_id]['quantity'] == 0
    	]);
    }

}
