<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function Login()
    {
		return view('public.login');
    }

    public function LoginPost(Request $request)
    {
    	
    	$credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {            
            return redirect()->intended('admin');
        }

    }

    public function Logout()
    {
		Auth::logout();
		return redirect()->route('home');
    }
}
