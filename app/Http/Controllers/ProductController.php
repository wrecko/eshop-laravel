<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\ProductCategory;
use App\Models\OptionType;
use App\Models\Option;
use App\Models\OptionRelation;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductQuantity;

use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function Index()
    {
    	 return view('admin.product.index', [
    	 	'products' => Product::all()
    	 ]);
    }

    public function Create()
    {
    	$option_type_names = OptionType::all()->pluck('id','name');
    	$relation_names = OptionRelation::all()->pluck('id','name');

    	foreach ($relation_names as $relation => $val) {    		
    		$relation_names[$relation] *= 100;    		
    	}
    	
    	$option_type_names = $option_type_names->merge($relation_names);    	

    	return view('admin.product.create', [
    		'categories'		=> ProductCategory::all(),
    		'option_type_names'	=> $option_type_names,
    		'options'			=> Option::all(),
    		'create_session_id'	=> Str::random(32)
    	]);
    }

    public function CreatePost(Request $request)
    {
    	// var_dump($request->all());    
        // dd();

    	$model = new Product($request->all());
    	$active = $request->has('active');
    	$model->active = $active;

        //  Price check
        // if($model->price + ($model->price - ({{config('global.VAT')}} / 100)) != $model->price_vat)
        //     return;
    	
        try {
            if($model->save())
            {
                //  Images          
                $id = $model->id;
                $updatedRows = ProductImage::where('session_id' , $request->create_session_id)
                ->update([
                    'product_id'    => $id,
                    'session_id'    => null
                ]);

                if($updatedRows > 0)
                {
                    rename(
                        public_path().'\\'.config('global.IMAGE_UPLOAD_PATH').$request->create_session_id, 
                        public_path().'\\'.config('global.IMAGE_UPLOAD_PATH').$id
                    );  
                }
                
                //  Quantities

                $quantities = $request->Quantities;
                foreach ($quantities as $optionType => $option) {               
                    foreach ($option as $optionId => $value) 
                    {    
                        if($value == null) continue;                    
                        ProductQuantity::create([
                            'option_id'             => $optionId,
                            'product_id'            => $id,
                            'parent_option_id'      => $optionType,
                            'relation_option_id'    => $request->OptionRelationId,
                            'quantity'              => $value == null? 0 : $value
                        ]);
                    }
                }    
            }  
        } catch (\Exception $e) {
            var_dump($e);
            return;
        }

    	return redirect()->route('products');
    }


    public function AddOptionsList(Request $request)
    {
    	return response()->json($this->GetOptionList($request->optionTypeId));
    }


    //	TODO: REFACTOR - CHECK SQL CALLS
    public function Update($id)
    {
    	$product = Product::findorfail($id);
    	$option_type_names = OptionType::all()->pluck('id','name');
    	$relation_names = OptionRelation::all()->pluck('id','name');

    	foreach ($relation_names as $relation => $val) {    		
    		$relation_names[$relation] *= 100;    		
    	}

    	$savedQuantities = $product->quantities;            
    	if(isset($savedQuantities[0]->relation_option_id))
    		$optionList = $this->GetOptionList($savedQuantities[0]->relation_option_id);
    	else
    		$optionList = $this->GetOptionList($savedQuantities[0]->parent_option_id);

        
    	
		$result = [];
		
    	foreach ($optionList[0] as $type) 
    	{
    		$type_id = $type->id;
    		$item = (object)[
    			'id'	=> $type->id,
    			'name'	=> $type->name,
    			'options' => []
    		];
    		
    		foreach ($optionList[1] as $key => $option) 
    		{   
                $opt = (object)[
                            'quantity_id'   => 0,
                            'option_id'     => $option->id,
                            'option_type_id'=> $option->option_type_id,
                            'name'          => $option->name,
                            'value'         => 0
                        ];

    			foreach ($savedQuantities as $value) 
    			{   
                    if(	$value->parent_option_id == $type_id && $value->option_id == $option->id)    				
    				{
                        $opt->value = $value->quantity;               
                        $opt->quantity_id  = $value->id;
                    }

    			}
                $item->options[] = $opt;
    		}
    		$result[] = $item;
    	}
    	return view('admin.product.edit', [
    		'product'			=> $product,
    		'categories'		=> ProductCategory::all(),
    		'option_type_names'	=> $option_type_names,
    		'saved_options'		=> $result,
    		'create_session_id'	  => Str::random(32),
            'relation_option_id'  => $savedQuantities[0]->relation_option_id
    	]);
    }

    public function UpdatePost(Request $request)
    {
    	// var_dump($request->all());
        // dd();

    	$product = Product::find($request->id);
    	//	Product Info

    	$active = $request->has('active');
    	$product->active 		= $active;
    	$product->name 			= $request->name;
    	$product->description 	= $request->description;
    	$product->category_id 	= $request->category_id;
    	// $product->price 		= $request->price;
    	$product->price_vat		= $request->price_vat;
        $product->weight        = $request->weight;

    	if($product->save())
    	{
    		//	Saved Images	
	    	$formImgs = isset($request->images)? $request->images : [];
	    	foreach ($product->images as $key => $image) 
	    	{    		
	    		if(!in_array($image->id, $formImgs ))
	    		{    			
	    			$image->delete_with_files();
	    		}
	    	}

	    	//	New Images
	    	ProductImage::where('session_id' , $request->create_session_id)
	    		->update([
	    			'product_id'	=> $product->id,
	    			'session_id'	=> null
	    		]);

	    	//	Options
	    	// TODO: refactor - fix multiple sql calls
            $quants_to_update = [];            
	    	foreach ($request->quantities as $key => $value) 
            {               

	    		$quants_to_update[] = [
                    'id'            => $value['id'],
                    'quantity'      => $value['value'],
                    'product_id'    => $product->id,
                    'option_id'         => $value['option_id'],
                    'parent_option_id'  => $value['option_type_id'],
                    'relation_option_id'    => $request->relation_option_id
                ];
	    	}
            // var_dump($quants_to_update);
            ProductQuantity::upsert($quants_to_update, ['id'], ['quantity']);

    	}

    	return redirect()->route('products');
    }

    public function Delete($id)
    {
    	if($id != null && is_numeric($id))
   		{
   			Product::destroy($id);
   		}

   		return redirect()->back();
    }


    private function GetOptionList($option_type)
    {
    	//	Option relation
    	if($option_type % 100 == 0){
    		$relation = OptionRelation::find($option_type/100);
    		if($relation == null)
    			return response()->json(['error' => "Relation not found"]);

    		$parentOptions = Option::where('option_type_id', $relation->parentOptionId)->get();
    		$childOptions = Option::where('option_type_id', $relation->childOptionId)->get();

    		return [$parentOptions, $childOptions];
    	}

    	$option_type_obj = OptionType::find($option_type);
    	$options = Option::where('option_type_id', $option_type)->get();

    	return [[$option_type_obj], $options];
    }
}
