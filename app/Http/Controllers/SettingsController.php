<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transport;
use App\Models\TransportFee;

class SettingsController extends Controller
{
    public function index()
    {
    	return view('admin.settings.index',
    		[
    			'transports'	=> Transport::withCount('fees')->get()
    		]
    	);
    }

    public function save_fees(Request $request)
    {
    	
    	try
    	{
    		foreach ($request->all()['fees'] as $key => $value) 
    		{    
	    		TransportFee::where('id', $key)->update([
	    			'free_of_charge'	=> isset($value['free_of_charge'])? 1 : 0,
	    			'price' 			=> isset($value['price'])? $value['price'] : 0
	    		]);
    		}
    	}
    	catch (\Exception $e) {
            var_dump($e);
            return;
        }

    	return redirect()->back();
    }
}
