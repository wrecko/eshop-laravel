<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductImage;

class ImageStoreController extends Controller
{
	private $image_upload_path;

	function __construct()
	{
		$this->image_upload_path = config('global.IMAGE_UPLOAD_PATH');
	}
	

    public function storeImages(Request $request)
    {
        $move_dir = isset($request->product_id)? $request->product_id : $request->create_session_id;

        $images = $request->file('file');
        foreach ($images as $key => $value) {
        	$imageName = $value->getClientOriginalName();
        	$value->move(public_path($this->image_upload_path . $move_dir), $imageName); 

            $model = new ProductImage();
            $model->name = $imageName;
            // $model->is_thumbnail = 0;
            
            $model->session_id = $request->create_session_id;
            $model->save();
        }

        return response()->json(['asd' => '']);
    }

    public function deleteimages(Request $request)
    {
        $id = $request->id;
        
        $path=public_path(). '\\' . $this->image_upload_path . $id . '\\' .$request->name;
        if (file_exists($path)) {
            ProductImage::where('session_id', isset($request->session_id)? $request->session_id : $id)
            ->where('name', $request->name)
            ->delete();
            
            unlink($path);    

        }

        return $request->session_id;  
    }
}
