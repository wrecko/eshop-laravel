<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\ProductCategory;
use App\Models\Product;
use App\Models\Option;
use App\Models\ProductQuantity;
use App\Models\State;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Http\Requests\OrderRequest;
use App\Lib\Cart;


class EshopController extends Controller
{
    
    public function index()
    {
    	return view('public.eshop.index',[
    		'categories' => ProductCategory::has('active_products')->get()
    	]);
    }

    public function detail($id)
    {
    	$product = Product::findOrFail($id);
    	$quant = $product->quantities->where('quantity', '>', 0);
    	$parent = $quant->whereNotNull('relation_option_id')->unique('parent_option_id');    	
    	
    	return view('public.eshop.detail',
		[
    		'prod' 				=> $product,
    		'parent_options' 	=> $parent,
    		'quantities'		=> count($parent) > 0? $quant->where('parent_option_id', $parent->first()->parent_option_id) : $quant
    	]);
    }

    public function checkout()
    {
    	$cart = Cart::getCart();
    	$states = State::all();

    	return view('public.eshop.checkout',
		[
    		'cart' 			=> $cart['products'],
    		'states' 		=> $states,
    		'price_total'	=> $cart['total_price']
    	]);
    }

    public function checkout_post(OrderRequest $request)
    {

	   	
	   	$cart = Cart::getCart();   

        $order = new Order($request->all());        
	   	$order->total_price_vat = $cart['total_price'];
	   	$order->total_price = /*$total-($total*0.2)*/ 0;	   	
	   	$order->street = $request->street . ' ' . $request->house_number;

	   	$order->phone_number = State::find($order->state)->phone_prefix . $order->phone_number;


	   	if($order->save())
	   	{
	   		$prods = [];   		
	   		
	   		$quant_models = ProductQuantity::whereIn('product_id', array_keys($cart['products']))->get();
	   		if($quant_models->isEmpty())
	   			//	TODO: return error
	   			return;

	   		// var_dump($quant_models);
		   	foreach ($cart['products'] as $prod_id => $prod) 
		   	{
		   		foreach ($prod['options'] as $quantity_id => $option) 
		   		{		   			
		   			//	CREATE MODEL
		   			$prodModel = new OrderProduct();		   			
					$prodModel->product_id		= $prod_id;
					$prodModel->count 			= $option['quantity'];
					$prodModel->parent_option	= $option['parent_id'];
					$prodModel->option 			= $option['option_id'];

					$prods[] = $prodModel;

					//	DECREMENT COUNT
					$quant_models->find($quantity_id)->quantity -= $prodModel->count;

		   		}
		   	}

		   	// TODO: try sql procedure do decrement count ? ?????? ??
		   	if($order->order_products()->saveMany($prods))
		   	{
		   		foreach ($quant_models as $key => $value) {
		   			if($value->isDirty('quantity'))
		   				$value->save();
		   		}				
				// var_dump($order);

				//  CLEAR CART
                Cart::clearCart();

                // SEND MAIL
                Mail::to($order->email)->send(new \App\Mail\OrderCreated($order));
			  	return view('public.eshop.checkout_success');
		   	}

		}
		return view('public.eshop.checkout',
		[
    		'cart' 			=> $cart['products'],
    		'states' 		=> State::all(),
    		'price_total'	=> $cart['total_price']
    	])->withErrors();
    }
    
    public function get_options_for_parent(Request $request)
    {
    	$quant = ProductQuantity::with('option')
		->where('parent_option_id', $request->parent_id)
    	->where('product_id', $request->product_id)
    	->where('quantity', '>', 0)
    	->get()
    	->pluck('id', 'option.name');

    	return response()->json(['options' => $quant]);
    }
}
