<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
            'street' => 'required|max:255',
            'house_number' => 'required|max:255',
            'zip' => 'required|digits:5',
            'city' => 'required|max:50',
            'state' => 'required|numeric|exists:state,id',
            'email' => 'required|email',
            'phone_number' => 'numeric',
            'note' => 'max:255',
            'payment' => 'required|numeric',
            'transport' => 'required|numeric'        
        ];
    }

    protected function prepareForValidation()
    {
        
        $this->merge(['phone_number' => str_replace(' ', '', $this->phone_number)]);
    }
}
