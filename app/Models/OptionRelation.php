<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OptionRelation extends Model
{
    use HasFactory;

    protected $table = 'option_relation';
    protected $fillable = ['parentOptionId', 'childOptionId', 'name'];

    public function parent_type()
    {
    	return $this->hasOne('App\Models\OptionType', 'id', 'parentTypeId');
    }

    public function child_type()
    {
    	return $this->hasOne('App\Models\OptionType', 'id', 'childTypeId');
    }
}
