<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory;

    protected $table = 'product_category';
    protected $fillable = ['name', 'parentcategoryid'];

    public function getParentAttribute()
    {
    	$parent = $this->find($this->parentCategoryId);    	
    	if($parent != null)
    		return $parent->name;
    	return '-';
    }
    public function getParentidAttribute()
    {    	
    	return $this->parentCategoryId == null? -1: $this->parentCategoryId;
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id');
    }

    public function active_products()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id')->where('active', 1);
    }
}
