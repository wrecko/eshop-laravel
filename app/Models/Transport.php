<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    use HasFactory;
    protected $table = 'transport_method';

    public function fees()
    {
    	return $this->hasMany('App\Models\TransportFee', 'transport_id', 'id');
    }
}
