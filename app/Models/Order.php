<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Enums\OrderStatus;


class Order extends Model
{
    use HasFactory;

 	protected $table = 'order';
    protected $fillable = ['name', 'surname', 'street', 'zip', 'city', 'state', 'email', 'phone_number', 'note', 'total_price', 'total_price_vat', 'payment', 'transport', 'order_status'];

    protected $state;

    // protected $maps = [ 'state' => 'state_id'];
    protected $append = ['state_id'];
    

    public function state()
    {
    	return $this->belongsTo('App\Models\State', 'state_id', 'id');
    }
    public function order_products()
    {
    	return $this->hasMany('App\Models\OrderProduct', 'order_id', 'id');
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->name) . ' ' . ucfirst($this->surname);
    }

    public function getFullAddressAttribute()
    {
        return "{$this->street}, {$this->zip} {$this->city}";
    }

    public function getOrderStatusNameAttribute()
    {        
        return OrderStatus::toString($this->order_status);
    }

    public function getOrderStatusClassAttribute()
    {       
        
        switch ($this->order_status)
        {
            case OrderStatus::NEW: return "btn-secondary";
            case OrderStatus::ACCEPTED: return "btn-success"; 
            case OrderStatus::EXPEDITED: return  "btn-warning"; 
            case OrderStatus::CLOSED: return  "btn-info"; 
            case OrderStatus::INVALID: return "btn-danger"; 
            default:
                return "btn-secondary";
        }
    }

    //  TODO: 
    // public function getPhoneNumberWithPrefixAttribute()
    // {   
    //     return "+{$this->state()->get()->phone_prefix} {$this->phone_number}";
    // }

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->attributes['state_id'] = $model->attributes['state'];
            unset($model->attributes['state']);
        });
    }
}
