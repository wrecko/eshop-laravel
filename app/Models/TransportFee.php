<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransportFee extends Model
{
    use HasFactory;

    protected $table = 'transport_fee';
    // protected $fillable = ['transport_id', 'state_id', 'price'/*'from_weight', 'to_weight',*/];
    protected $casts = [
        'free_of_charge' => 'boolean'
    ];


    public function transport()
    {
    	return $this->belongsTo('App\Models\Transport', 'transport_id', 'id');
    }

    public function state()
    {
    	return $this->hasOne('App\Models\State', 'id', 'state_id');	
    }

    public function GetStateNameAttribute()
    {
    	$state = $this->state;
    	return $this->state == null ? "" : $state->name;
    }
}
