<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductQuantity extends Model
{
    use HasFactory;

    protected $table = 'product_quantity';
    protected $fillable = ['option_id', 'product_id', 'parent_option_id', 'relation_option_id', 'quantity'];

    public function option()
    {
    	return $this->belongsTo('App\Models\Option', 'option_id', 'id');
    }

    public function parent_option()
    {
    	return $this->hasOne('App\Models\Option', 'id', 'parent_option_id');
    }

    
}
