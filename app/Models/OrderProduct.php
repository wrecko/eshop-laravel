<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    use HasFactory;

    protected $table = 'order_product';


    public function order()
    {
    	return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }


    //    TODO: do another model with sql 'join'?????
    public function product()
    {
    	return $this->hasOne('App\Models\Product',  'id', 'product_id');
    }
    
}
