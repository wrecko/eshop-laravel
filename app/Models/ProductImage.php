<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;

    protected $table = 'product_image';
    protected $fillable = ['product_id', 'name', 'is_thumbnail', 'session_id'];

    public function getPathAttribute()
    {
        return asset(config('global.IMAGE_UPLOAD_PATH') . $this->product_id . '\\' .$this->name);
    }

    public function get_image_path()
    {
    	// return public_path() . '\\' . config('global.IMAGE_UPLOAD_PATH') .$this->product_id . '\\' .$this->name;
    	return asset(config('global.IMAGE_UPLOAD_PATH') . $this->product_id . '\\' .$this->name);
    }

    public function get_image_absolute_path()
    {
    	return public_path() . '\\' . config('global.IMAGE_UPLOAD_PATH') .$this->product_id . '\\' .$this->name;
    }

    public function delete_with_files()
    {        
        try 
        {
            $this->delete();
            unlink($this->get_image_absolute_path());
        } 
        catch (Exception $e) 
        {
            // TODO: audit exception
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}
