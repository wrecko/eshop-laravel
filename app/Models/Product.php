<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'product';
    protected $fillable = ['name', 'category_id', 'description', 'price', 'price_vat', 'weight', 'active'];
    protected $casts = [
        'active' => 'boolean'
    ];

    public function images()
    {
    	return $this->hasMany('App\Models\ProductImage','product_id', 'id');
    }

    public function category()
    {
    	return $this->belongsTo('App\Models\ProductCategory', 'category_id', 'id');
    }

    public function quantities()
    {
        return $this->hasMany('App\Models\ProductQuantity', 'product_id', 'id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($prod) { 

            foreach ($prod->images as $img) {
                $img->delete_with_files();
            }
            $prod->quantities()->delete();
             
        });
    }
}
