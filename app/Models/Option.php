<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasFactory;

    protected $table = 'option';
    protected $fillable = ['name', 'option_type_id'];

    public function type()
    {
    	return $this->belongsTo('App\Models\OptionType', 'option_type_id', 'id');
    }
}
