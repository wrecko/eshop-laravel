<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OptionType extends Model
{
    use HasFactory;

    public static $rules = [
    	'name' => 'required|max:255'
    ];
        

    protected $table = 'option_type';
    protected $fillable = ['name'];

    public function options()
    {
    	return $this->hasMany('App\Models\Option', 'option_type_id', 'id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($type) { 

             $type->options()->delete();
             
        });
    }
    
}
