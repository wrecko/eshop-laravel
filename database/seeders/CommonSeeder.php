<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('state')->insert(
        [
            [
            	'name' 			=> 'Slovensko',
            	'phone_prefix'	=> '421'
        	],
            [
            	'name' 			=> 'Česko',
            	'phone_prefix'	=> '420'
        	],
        ]);

        DB::table('payment_method')->insert(
        [
            [
                'name'              => 'cash on delivery',
                'localization_name' => 'cash_on_delivery',
                'active'            => 1
            ],
            [
                'name'              => 'transfer to an account',
                'localization_name' => 'transfer_to_an_account',
                'active'            => 1
            ]
        ]);

        DB::table('transport_method')->insert(
        [
            [
                'name'              => 'personal pickup',
                'localization_name' => 'personal_pickup ',
                'active'            => 1
            ],
            [
                'name'              => 'post service',
                'localization_name' => 'post_service',
                'active'            => 1
            ]
        ]);

        DB::table('transport_fee')->insert(
        [
            [
                'transport_id'      => 2,
                'state_id'          => 1,
                'price'             => 3.5
            ],
            [
                'transport_id'      => 2,
                'state_id'          => 2,
                'price'             => 5.00
            ]
        ]);

        DB::table('transport_fee')->insert(
        [          
            [
                'transport_id'      => 1,
                'free_of_charge'    => true,
                'state_id'          => null
            ]
        ]);
    }
}
