<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_category')->insert([
        	[
    			'name' 	=>	'Tričko'        
            ],
            [
                'name'  =>  'Šálka'        
            ]
        ]);   



        DB::table('product')->insert(
            [
                'name'          => 'Testovaci produkt',
                'category_id'   => '1',
                'description'   => 'skusame testovaci produkt',
                'price'         => '8.79',
                'price_vat'     => '10.99',
                'weight'        => '120',
                'active'        => 1
            ]
        );

        DB::table('product_quantity')->insert([
            [
                'option_id' => 1,
                'product_id' => 1,
                'parent_option_id' => 1,
                'relation_option_id' => null,
                'quantity' => 999
            ],
            [
                'option_id' => 2,
                'product_id' => 1,
                'parent_option_id' => 1,
                'relation_option_id' => null,
                'quantity' => 0
            ],
        ]);

        DB::table('product_image')->insert([
            [
                'product_id'    => 1,
                'name'          => 'cod_1.jpg',
                'is_thumbnail'  => 0,
                'session_id'    => null
            ],
            [
                'product_id'    => 1,
                'name'          => 'cod_2.jpg',
                'is_thumbnail'  => 0,
                'session_id'    => null
            ]
        ]);

    }
}
