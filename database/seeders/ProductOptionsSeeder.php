<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductOptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('option_type')->insert(
        [
            ['name' 	=> 'Farba'],
            ['name'     => 'Velkost']
        ]);

        DB::table('option')->insert(
        [
            [
                'name'     => 'Čierna',
                'option_type_id' => 1
            ],
            [
                'name'     => 'Biela',
                'option_type_id' => 1
            ],
            [
                'name'     => 'S',
                'option_type_id' => 2
            ],
            [
                'name'     => 'M',
                'option_type_id' => 2
            ],
            [
                'name'     => 'L',
                'option_type_id' => 2
            ],
        	
        ]);

        DB::table('option_relation')->insert(
        [
            'id'                =>  1,
            'name'              => 'Farba-Velkost',
            'parentOptionId'    => 1,
            'childOptionId'     => 2
        ]);
    }
}
