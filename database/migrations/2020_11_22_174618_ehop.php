<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ehop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_type', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('categoryId')->nullable();
            $table->timestamps();
        });

        Schema::create('option', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            // $table->unsignedBigInteger('optionTypeId');
            $table->foreignId('option_type_id')->constrained('option_type');
            $table->timestamps();
        });

        Schema::create('option_relation', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('parentOptionId')->constrained('option_type');
            $table->foreignId('childOptionId')->constrained('option_type');
            $table->timestamps();
        });  


        Schema::create('product_category', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedInteger('parentCategoryId')->nullable();
            $table->timestamps();
        });        
        
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('category_id')->constrained('product_category');
            $table->longText('description')->nullable();
            $table->decimal('price')->nullable();
            $table->decimal('price_vat');
            $table->decimal('weight');
            $table->boolean('active');
            $table->timestamps();
        });    

        Schema::create('product_image', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('product_id')->nullable();
            $table->string('name');
            $table->boolean('is_thumbnail')->default(false);
            $table->string('session_id')->nullable();
            $table->timestamps();
        });  

        Schema::create('product_quantity', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('option_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('parent_option_id')->nullable();
            $table->unsignedInteger('relation_option_id')->nullable();
            $table->integer('quantity');
            $table->timestamps();
        });  

        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname');
            $table->string('street');
            $table->string('zip');
            $table->string('city');
            $table->foreignId('state_id')->constrained('state');
            $table->string('email');
            $table->string('phone_number');
            $table->longText('note')->nullable();
            $table->decimal('total_price');
            $table->decimal('total_price_vat');

            $table->foreignId('payment')->constrained('payment_method');
            $table->foreignId('transport')->constrained('transport_method');;
            $table->unsignedInteger('order_status')->default('1');            
            $table->timestamps();
        });  

        Schema::create('order_product', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->constrained('order');
            $table->foreignId('product_id')->constrained('product');
            $table->integer('count');           
            $table->unsignedInteger('parent_option')->nullable();
            $table->unsignedInteger('option')->nullable();            
            $table->timestamps();
        });  

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_type');
        Schema::dropIfExists('option');
    }
}
