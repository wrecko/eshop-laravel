<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Common extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('operation')->default(0);
            $table->longText('msg')->default(0);
            $table->timestamps();
        });
        Schema::create('state', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('phone_prefix');
            $table->timestamps();
        });

        Schema::create('payment_method', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('localization_name');
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('transport_method', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('localization_name');
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('transport_fee', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transport_id')->constrained('transport_method');
            // $table->integer('from_weight')->default(0);
            // $table->integer('to_weight')->default(0);
            $table->foreignId('state_id')->nullable()->constrained('state');
            $table->decimal('price')->default(0);
            $table->boolean('free_of_charge')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('state');
        Schema::dropIfExists('payment_method');
        Schema::dropIfExists('transport_method');
    }
}
