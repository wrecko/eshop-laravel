@extends('admin._layout')

@section('content')


<div class="admin-category-add">
    <h2>Orders</h2>
    <input type="text" placeholder="Search..." class="form-control mx-sm-3" id="search" />
</div>

<table class="table table-striped table-dark">
    <thead>
        <tr>
            <th scope="col">Order #</th>
            <th scope="col">Name</th>
            <th scope="col">Address</th>
            <th scope="col">Price(VAT)</th>
            <th scope="col">Order Status</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        
        @foreach($orders as $order)
        
            <tr>
                <td>{{$order->id}}</td>
                <td>{{$order->full_name}}</td>
                <td>{{$order->full_address}}</td>
                <td>{{$order->total_price_vat}}&euro;</td>
                <td>
                    <span class="btn {{$order->order_status_class}}" style="cursor: default;">{{$order->order_status_name}}</span>
                </td>
                <td style="text-align:center;">
                    <a href="{{route('order_detail',['id' => $order->id])}}" class="btn btn-warning">DETAIL</a>
                    
                </td>
            </tr>
        @endforeach

        

    </tbody>
</table>

@endsection