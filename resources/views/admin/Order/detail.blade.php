@extends('admin._layout')

@section('content')



<h1>Order #{{$order->id}}</h1>
<hr />
<div class="admin-order-detail">
    <div class="order-customer">
        <h3>Customer</h3>
        <hr />
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="fullname" class="">Full name:</label>
                    <input type="text" value="{{$order->full_name}}" class="form-control" disabled />
                </div>
                <div class="form-group">
                    <label for="fullname" class="">Address:</label>
                    <input type="text" value="{{$order->full_address}}, {{$order->state->name}}" class="form-control" disabled />
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="fullname" class="">Phone number:</label>
                    <input type="text" value="+{{$order->state->phone_prefix}} {{$order->phone_number}}" class="form-control" disabled />
                </div>
                <div class="form-group">
                    <label for="fullname" class="">Email address:</label>
                    <input type="text" value="{{$order->email}}" class="form-control" disabled />
                </div>
            </div>
        </div>        
    </div>
    <div class="order-detail">
        <div class="row">
            <div class="col">
                <h3>Products</h3>
                <hr />
                <div class="order-products">
                    @foreach($order->order_products as $prod)
                    
                        <div class="prod">
                            <div class="prod-img">
                                <img src="{{$prod->product->images->first()->path}}" alt="{{$prod->product->name}}" />
                            </div>
                            <div class="prod-detail">
                                <span>{{$prod->product->name}}</span>
                                <br />
                                <span><!-- prod.ParentOptionName prod.OptionName --></span>
                                <span>{{$prod->count}} pcs.</span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col">
                <h3>Details</h3>
                <hr />
                <div class="price-total">
                    <span class="hdr">Price Total: </span>                    
                    <span style="font-size: 18px;">{{$order->total_price_vat}}&euro; ({{$order->total_price}}&euro; w/o VAT)</span>
                </div>

                <div class="order-status m-t-10">
                    <span class="hdr">Order status:</span>
                    <span style="font-weight: 600; text-transform: uppercase;">{{$order->order_status_name}}</span>
                    <br />
                    <br />
                    <span class="hdr">Change status to:</span>
                    <div class="row m-t-10" style="text-transform: uppercase;">
                        <div class="col">
                            <a href="{{route('change_status', ['id' => $order->id, 'status' => App\Enums\OrderStatus::ACCEPTED])}}" class="btn btn-success">{{App\Enums\OrderStatus::toString(App\Enums\OrderStatus::ACCEPTED)}}</a>
                            
                        </div>
                        <div class="col">
                            <a href="{{route('change_status', ['id' => $order->id, 'status' => App\Enums\OrderStatus::EXPEDITED])}}" class="btn btn-warning">{{App\Enums\OrderStatus::toString(App\Enums\OrderStatus::EXPEDITED)}}</a>
                           
                        </div>
                        <div class="col">
                            <a href="{{route('change_status', ['id' => $order->id, 'status' => App\Enums\OrderStatus::CLOSED])}}" class="btn btn-info">{{App\Enums\OrderStatus::toString(App\Enums\OrderStatus::CLOSED)}}</a>
                           
                        </div>
                        <div class="col">
                            <a href="{{route('change_status', ['id' => $order->id, 'status' => App\Enums\OrderStatus::INVALID])}}" class="btn btn-danger">{{App\Enums\OrderStatus::toString(App\Enums\OrderStatus::INVALID)}}</a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

@endsection