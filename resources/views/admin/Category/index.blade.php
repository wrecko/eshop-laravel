@extends('admin._layout')

@section('content')

<div class="admin-category-add">
    <h2>Categories</h2>    
    <hr />    
    <div class="row" style="padding-left: 15px;">
        <form action="{{ route('category_create') }}" method="post" id="CategoryCreate"> 
            @csrf   
            <div class="row">
                <div class="col-sm">
                    <label for="OptionTypes">Parent Category:</label>
                    <select class="form-control" id="parent-id-create" name="parentcategoryid">
                        <option value="-1">None</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>                
                </div>
                <div class="col-sm">
                    <label id="name" name="name">Name: </label>
                    <input type="text" name="name" id="category-name-create" class="form-control" />                
                </div>
                <div class="col-sm" style="display: inline-flex;">
                    <input type="submit" class="btn btn-success" value="Create" id="product-category-create-btn" style="" />
                    <input type="button" class="btn btn-warning" value="Edit" id="product-category-edit-btn" style=" display:none;" />
                    <input type="button" class="btn btn-danger" value="Cancel" id="product-category-cancel-btn" style="display:none;" />
                </div>
            </div>
       
        </form>
    </div>
    <hr /> 
   
</div>

<input type="text" placeholder="Search..." class="form-control" style="width: 20%; float: right; margin-bottom: 10px" id="search" />
<table class="table table-striped table-dark">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Category</th>
            <th scope="col">Parent</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($categories as $category)
        
            <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->name}}</td>
                <td>{{$category->parent}}</td>
                <td>
                    <a href="#" onclick="EditOption('{{$category->name}}', null, '{{$category->id}}', '{{$category->parentId}}')" class="btn btn-warning">Edit</a>
                    <a href="#" onclick="window.confirm('Are you sure?')? window.location.href = '{{ route('category_delete', ['id'=>$category->id])}}': ''" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        @endforeach     
    </tbody>
</table>

@endsection


@section('BottomScripts')
<script>
    $("#product-category-edit-btn").on("click", function (e) {
        e.preventDefault();
        $('#CategoryCreate').attr('action', "{{route('category_update')}}").submit();
    });
        
</script>
@endsection