@extends('admin._layout')

@section('content')

<div class="header-text">
	<h2>Settings</h2>
	<input type="button" id="save_fee_btn" class="btn btn-success top-submit-btn" value="SAVE">
</div>
<hr>

<div class="weight-transport-section">
	<h3> Transport fees </h3>
	<hr>

	<form action="{{ route('save_fees') }}" method="post", id="fees-form">  
	@csrf()
		<div class="fee-table">
			
			@foreach($transports as $transport)			
				@if($transport->fees_count == 0)
					@continue
				@endif
				<div id="transport-{{$transport->id}}">
					<span class="transport-name">{{$transport->name}}</span>

					<table class="table">
					 <thead>
				        <tr>		            
				            <!-- <th scope="col">From (g)</th>
				            <th scope="col">To (g)</th> -->
				            <th scope="col">State name</th>
				            <th scope="col">Price</th>
				            <th scope="col">Free of charge</th>
				        </tr>
				    </thead>
			    	<tbody>   
			    		@foreach($transport->fees as $fee)
			    			<tr>
			    				<!-- <td>
			    					<input type="text" class="form-control" name="fees[{{$transport->id}}][from_weight]" value="{{$fee->from_weight}}" 
			    					@if($fee->free_of_charge)
					                    disabled
					                @endif>
			    				</td>
			    				<td>
			    					<input type="text" class="form-control" name="fees[{{$transport->id}}][to_weight]" value="{{$fee->to_weight}}"
			    					@if($fee->free_of_charge)
					                    disabled
					                @endif>
			    				</td> -->
			    				<td>
			    					<span>{{$fee->state_name}}</span>
			    				</td>
			    				<td>
			    					<input type="text" class="form-control" name="fees[{{$fee->id}}][price]" value="{{$fee->price}}"
			    					@if($fee->free_of_charge)
					                    disabled
					                @endif>
			    				</td>
			    				<td>
			    					<input type="checkbox" class="free_of_charge_check" name="fees[{{$fee->id}}][free_of_charge]"
					                @if($fee->free_of_charge)
					                    checked
					                @endif>
			    					<!-- <input type="hidden" name="fees[{{$transport->id}}][id]" value="{{$fee->id}}"> -->
			    				</td>
			    			</tr>
			    		@endforeach
			    	</tbody>
				</table>
				</div>
			
			@endforeach
			
		</div>

		<div class="add-fee">
			<div class="row">
				<!-- <div class="col-md-6">
					<select class="form-control">
						@foreach($transports as $trans)
							<option value="{{$trans->id}}">{{$trans->name}}</option>
						@endforeach
					</select>
				</div> -->
				<div class="col-md-6">
					<!-- <input type="button" class="btn btn-success" value="ADD"> -->					
				</div>
				
			</div>
		</div>
	</form>
</div>


@endsection

@section('BottomScripts')
<script type="text/javascript">
	$('.free_of_charge_check').change(function(){
		var inputs = $(this).parents('tr').find('input:text');
		if(this.checked) 
		{
			$.each(inputs, function(i,val){
				 $(val).prop("disabled", true);
			})
		}
		else
		{
			$.each(inputs, function(i,val){
				 $(val).prop("disabled", false);
			})
		}
		
	})	



</script>
@endsection