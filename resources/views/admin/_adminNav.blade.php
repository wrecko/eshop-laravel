

<nav class='admin-nav'>
    <ul class="menu">
        <li class="menu-item">
            <a href="">
                <span>Products</span>
            </a>

            <div class="admin-nav-submenu">
                <ul>
                    <!-- <li>Html.ActionLink("Categories", "index", "Category")</li>
                    <li>Html.ActionLink("Products", "index", "Product")</li>
                    <li>Html.ActionLink("Options", "Options", "Product")</li>
                    <li>Html.ActionLink("Orders", "Index", "Order")</li> -->
                    <li><a href="{{ route('categories') }}">Categories</a></li>
                    <li><a href="{{ route('products') }}">Products</a></li>
                    <li><a href="{{ route('options') }}">Options</a></li>                    
                    
                    
                </ul>
            </div>
        </li>
        <li class="menu-item"><a href="{{ route('orders') }}">Orders</a></li> 
        <li class="menu-item"><a href="{{ route('settings') }}">Settings</a></li> 
        <li class="menu-item"><a href="/logout">Logout</a></li>
    </ul>
</nav>