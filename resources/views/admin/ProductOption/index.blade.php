@extends('admin._layout')

@section('content')

<ul class="nav nav-tabs" id="myTab" role="tablist" style="position: relative;">
    <li class="nav-item">
        <a class="nav-link active" id="option-tab" data-toggle="tab" href="#option" role="tab" aria-controls="option" aria-selected="true">Options</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="option-type-tab" data-toggle="tab" href="#option-type" role="tab" aria-controls="option-type" aria-selected="false">Option Types</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="option-relations-tab" data-toggle="tab" href="#option-relations" role="tab" aria-controls="option-relations" aria-selected="false">Option Relations</a>
    </li>
</ul>

<div class="tab-content" id="myTabContent" style="padding: 15px 0;">
    <div class="tab-pane fade show active" id="option" role="tabpanel" aria-labelledby="option-tab">
        @include('admin.productoption._productOptions')        
    </div>

    <div class="tab-pane fade" id="option-type" role="tabpanel" aria-labelledby="option-type-tab">
        @include('admin.productoption._productOptionTypes')    
    </div>

    <div class="tab-pane fade" id="option-relations" role="tabpanel" aria-labelledby="option-relations-tab">
        @include('admin.productoption._productOptionRelations') 
    </div>
</div>

       
@endsection

@section('BottomScripts')

    <script>
               

        $("#product-option-edit-btn").on("click", function (e) {
            e.preventDefault();
            $('#OptionCreate').attr('action', "{{route('option_update')}}").submit();
        });

        $("#product-option-type-edit-btn").on("click", function (e) {
            e.preventDefault();
            $('#OptionTypeCreate').attr('action', "{{route('option_type_update')}}").submit();
        });
        
        //  Relations
        $("#ParentOptionId").on("change", function () {
            $("#ChildOptionId option").show();

            var val = $("#ParentOptionId option:selected").text()
            var child = $("#ChildOptionId option:selected").text()
            $("#ChildOptionId option:contains(" + val + ")").hide()

            $("#relation-name").val(val+"-"+child)
        })

         $("#ChildOptionId").on("change", function () {
            var val = $("#ParentOptionId option:selected").text()
            var child = $("#ChildOptionId option:selected").text()

            $("#relation-name").val(val+"-"+child)
        })

        $('document').ready(function () {
            var parent = $("#ParentOptionId option:selected").text()
            var child = $("#ChildOptionId option:selected").text()
            $("#ChildOptionId option:contains(" + parent + ")").hide()
            $("#relation-name").val(parent+"-"+child)
        })

    </script>

@endsection
