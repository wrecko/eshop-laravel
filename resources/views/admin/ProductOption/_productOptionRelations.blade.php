

<div class="product-option-create">
    <h2>Create</h2>
    <hr />
    <form action="{{ route('option_relation_create') }}" method="post", id="OptionRelationCreate"> 
        @csrf 
        <div class="row">
            <div class="col-sm">
                
                <select class="form-control" id="ParentOptionId" name="parentOptionId" class="form-control">
                    @foreach($optionTypes as $optionType)
                        <option value="{{$optionType->id}}">{{$optionType->name}}</option>
                    @endforeach
                </select> 
            </div>
            <div class="col-1 text-center">
                <img src="{{ asset('storage/icons/arrow-left-right.svg') }}" width="50%" />
            </div>
            <div class="col-sm">
                <select class="form-control" id="ChildOptionId" name="childOptionId">
                    @foreach($optionTypes as $optionType)
                        <option value="{{$optionType->id}}">{{$optionType->name}}</option>
                    @endforeach
                </select> 
            </div>
            <div class="col-sm">
                <input type="submit" class="btn btn-success" value="Create" id="product-option-type-create-btn" />
           
            </div>
            <input type="Hidden" name="name" id="relation-name">   
        </div>
    </form>
</div>

<div class="product-option-list">
    <h2>Relations</h2>
    <hr />
    <table class="table table-striped table-dark">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($relations as $relation)
            
                <tr>
                    <td>{{$relation->id}}</td>
                    <td>{{$relation->name}}</td>
                    <td>
    <a href="#" onclick="window.confirm('Are you sure?')? window.location.href = '{{ route('option_relation_delete', ['id' => $relation->id]) }}': ''" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>


