
<div class="product-option-create">
    <h2>Create / Edit</h2>
    <hr />
    <form action="{{ route('option_create') }}" method="post", id="OptionCreate"> 
        @csrf   
        <div class="row">
            <div class="col-sm">
                <label for="OptionTypes">Option Type:</label>
                <select class="form-control" id="option-type-create" name="option_type_id">
                    @foreach($optionTypes as $optionType)
                        <option value="{{$optionType->id}}">{{$optionType->name}}</option>
                    @endforeach
                </select>                
            </div>
            <div class="col-sm">
                <label id="name" name="name">Name: </label>
                <input type="text" name="name" id="option-name-create" class="form-control" />                
            </div>
            <div class="col-sm">
                <input type="submit" class="btn btn-success" value="Create" id="product-option-create-btn" style="margin-top: 30px;" />
                <input type="button" class="btn btn-warning" value="Edit" id="product-option-edit-btn" style="margin-top: 30px; display:none;" />
                <input type="button" class="btn btn-danger" value="Cancel" id="product-option-cancel-btn" style="margin-top: 30px; display:none;" />
            </div>
        </div>
   
    </form>
</div>

<div class="product-option-list">
    <h2>Options</h2>
    <hr />
    <table class="table table-striped table-dark">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Option Type</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($options as $option)
                <tr>
                    <td>{{$option->id}}</td>
                    <td>{{$option->name}}</td>
                    <td>{{$option->type->name}}</td>
                    <td>
                        <a href="#" onclick="EditOption('{{$option->name}}', '{{$option->optionTypeId}}', '{{$option->id}}')" class="btn btn-warning">Edit</a>
                        <a href="#" onclick="window.confirm('Are you sure?')? window.location.href = '{{ route('option_delete', ['id' => $option->id]) }}': ''" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>