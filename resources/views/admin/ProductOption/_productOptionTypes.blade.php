<div class="product-option-create">
    <h2>Create / Edit</h2>
    <hr />
    <form action="{{ route('option_type_create') }}" method="post", id="OptionTypeCreate">       
        @csrf
        <div class="row">
            <!-- <div class="col-sm">               
                <label id="Category" name="Category">Category: </label>
            </div> -->
            <div class="col-sm">
                <label id="Name" name="name">Name: </label>
                <input type="text" name="name" id="option-type-name-create" class="form-control" />                
            </div>
            <div class="col-sm">
                <input type="submit" class="btn btn-success" value="Create" id="product-option-type-create-btn" style="margin-top: 30px;" />
                <input type="button" class="btn btn-warning" value="Edit" id="product-option-type-edit-btn" style="margin-top: 30px; display:none;" />
                <input type="button" class="btn btn-danger" value="Cancel" id="product-option-type-cancel-btn" style="margin-top: 30px; display:none;" />
            </div>
        </div>
    
    </form>
</div>

<div class="product-option-list">
    <h2>Option types</h2>
    <hr />
    <table class="table table-striped table-dark">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>   
        @foreach($optionTypes as $optionType)         
            <tr>
                <td>{{$optionType->id}}</td>
                <td>{{$optionType->name}}</td>
                <td>
                    <a href="#" onclick="EditOption('{{$optionType->name}}', null, '{{$optionType->id}}')" class="btn btn-warning">Edit</a>
                    <a href="#" onclick="window.confirm('Are you sure?')? window.location.href = '{{ route('option_type_delete', ['id' => $optionType->id])}}': ''" class="btn btn-danger">Delete</a>
                </td>
            </tr>     
        @endforeach       
        </tbody>
    </table>
</div>