
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin Panel</title>
    
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <link rel="stylesheet" href="{{asset('storage/css/admin.css')}}" />
    @yield("HeadCss")

    
    @yield("HeadScripts")
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</head>
<body>
    <header></header>

    @include('admin._adminNav')
    <div class="container">
        @yield('content')
    </div>

    <!-- <script src="~/js/admin-scripts.js"></script> -->
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        function EditOption(Name, TypeId, Id, ParentId) {
            if(ParentId)
            {
                console.log(ParentId);
                $("#product-category-create-btn").hide();
                $("#product-category-edit-btn").show();
                $("#product-category-cancel-btn").show();
                $("#parent-id-create option[value=" + ParentId + "]").attr("selected", "selected");
                $("#category-name-create").val(Name);
                $("#CategoryCreate").append("<input type=\"hidden\" value=\"" + Id + "\" name=\"id\" id=\"ProductCategoryId\">")
            }
            else
            {
                var optionElm = "option-type";
                
                if (TypeId) {
                    optionElm = "option";
                    $("#option-type-create option[value=" + TypeId + "]").attr("selected", "selected");
                }
                    
                $("#"+optionElm+"-name-create").val(Name);

                $("#product-"+optionElm+"-create-btn").hide();
                $("#product-"+optionElm+"-edit-btn").show();
                $("#product-" + optionElm + "-cancel-btn").show();

                if (TypeId)
                    $("#OptionCreate").append("<input type=\"hidden\" value=\"" + Id + "\" name=\"id\" id=\"ProductOptionId\">")
                else
                    $("#OptionTypeCreate").append("<input type=\"hidden\" value=\"" + Id + "\" name=\"id\" id=\"ProductOptionId\">")
            }
        }

        $("input[id*=cancel-btn]").on("click", function (e) {
            e.preventDefault();
            
            $("#category-name-create").val("");

            $("#OptionTypeCreate input[type='text']").val("");
            $("#OptionCreate input[type='text']").val("");

            $("input[id*=create-btn]").show();
            $("input[id*=edit-btn]").hide();
            $("input[id*=cancel-btn]").hide();

            $("#ProductOptionId").remove();
        })

    </script>
    @yield('BottomScripts')
</body>
</html>
