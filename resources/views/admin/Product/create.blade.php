@extends('admin._layout')

@section ('HeadScripts')
    <script src="{{asset('storage/js/dropzone-5.7.0/dist/dropzone.js')}}"></script>
@endsection

@section('HeadCss')
    <link rel="stylesheet" href="{{asset('storage/js/dropzone-5.7.0/dist/dropzone.css')}}" />
@endsection

@section('content')


<ul class="nav nav-tabs" id="myTab" role="tablist" style="position: relative;">
    <li class="nav-item">
        <a class="nav-link active" id="product-tab" data-toggle="tab" href="#product" role="tab" aria-controls="product" aria-selected="true">Product</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="quantities-tab" data-toggle="tab" href="#quantities" role="tab" aria-controls="quantities" aria-selected="false">Quantities</a>
    </li>
    <li>        
        <input type="button" value="Save" class="btn btn-success top-submit-btn" onclick="$('#product-form').submit()" />        

    </li>
</ul>
<form action="{{ route('product_create') }}" method="post", id="product-form" enctype = "multipart/form-data">  
	@csrf()
	<div class="tab-content" id="myTabContent" style="padding: 15px 0;">
        <div class="tab-pane fade show active" id="product" role="tabpanel" aria-labelledby="product-tab">
            <h3>Description</h3>

            <div class="form-group">
            	<label for="active">Active: </label>
            	<input type="checkbox" name="active" style= "margin-left: 5px;" value="1">                
            </div>
            <div class="form-group">
            	<label for="name">Name: </label>
            	<input type="text" name="name" class="form-control" placeholder="Enter product name">
                
            </div>

            <div class="form-group">
            	<label for="category">Category: </label>
                
                <select class="form-control" id="product-category" name="category_id">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>

            </div>

            <div class="form-group">
            	<label for="description"> Description: </label>
                <input type="textarea" name="description" class="form-control" placeholder="Enter description" >                
                
            </div>

            <div class="row">
                <!-- <div class="col-sm">
                	<label for="price"> Price: </label>
                	<input type="text" name="price" class="form-control" id="product-price" placeholder="Enter price: ">
                    
                </div> -->
                <div class="col-sm">
                	<label for="pricevat">Price w/VAT ({{config('global.VAT')}}%): </label>
                    <input type="text" name="price_vat" class="form-control" id="product-price-vat" placeholder="Enter price: ">                   
                    
                </div>
                <div class="col-sm">
                    <label for="price"> Weight ({{config('global.WEIGHT_UNIT_SHORT')}}): </label>
                    <input type="text" name="weight" class="form-control" id="product-weight" placeholder="Enter weight: ">
                    
                </div>
            </div>


            <hr>

            <h3>Images</h3>
            <div id="product-dz" class="dropzone dz-clickable">
                <div class="fallback">
                    <input type="file" name="Images" multiple>
                </div>
            </div>
            <!-- if (Model.Images != null && Model.Images.Count > 0)
            {

                <h3 style="margin-top: 10px;">
                    Saved Images
                </h3>
                <div class="product-saved-imgs">

                    foreach (var item in Model.Images)
                    {
                        <div class="product-saved-img">
                            <img src="~/lib/bootstrap/dist/icons/x-circle.svg" class="delete" alt="" width="25" height="25" onclick="DeleteImage($(this).parent().remove())">
                            <img src="@item.Path" />
                            <input type="hidden" value="@item.Path" name="Images[@imageCounter].Path">
                            <input type="hidden" value="@item.Name" name="Images[@imageCounter].Name">
                            <input type="hidden" value="@item.Id" name="Images[@imageCounter].Id">
                            { imageCounter++; }
                        </div>
                    }
                </div>
            } -->



        </div>
        <div class="tab-pane fade" id="quantities" role="tabpanel" aria-labelledby="quantities-tab">
            @if(isset($option_type_names) && count($option_type_names) > 0)            
                <div style="width: 20%;display: flex; margin-bottom: 20px;">
                	<select name="option_types" id="option-type" class="form-control">
                		@foreach($option_type_names as $name => $id)
                		
                			<option value="{{$id}}">{{$name}}</option>
                		@endforeach
                	</select>
                    
                    <input type="button" id="optionAdd" class="btn btn-info" value="Add" style="margin-left: 10px;" />
                </div>
                <hr />
            @endif

            <!-- Saved product options -->
            <div id="product-create-types">                

            </div>
        </div>
    </div>

    
    <input type="hidden" name="create_session_id" value="{{$create_session_id}}">    
</form>

@endsection



@section('BottomScripts')
    <script>

        function DeleteImage(element) {
            $(element).parent().remove()
            RecalculateImageIds();            
        }

        function RecalculateImageIds() {
            var counter = 0;
            $("input[name*='Images']").each(function (index, val) {
                if (index != 0 && index % 3 == 0) {
                    counter++;
                }

                var inName = val.name;
                var inNum = inName.substring(inName.indexOf("[")+1, inName.indexOf("]"))                
                    console.log(inName,inNum )
                var tt = val.name.replace("["+inNum+"]", "["+counter+"]");
                $(val).attr("name", tt)
            })
        }

        
        $("#optionAdd").on("click", function () {

            var optionType = $("#option-type").val();
            var text = $( "#option-type option:selected" ).text();
            
            $.ajax({
                url: "{{route('add_options_list')}}",
                method: "POST",
                dataType: "json",
                data: { optionTypeId: optionType},
                success: function (data) {
                    console.log(data);                    
                    $("#product-create-types").html("");
                    $("input[name='OptionRelationId']").remove();
                    //$("#quantities").empty();
                    //$("#product-create-type").append("<span>" + text + "</span>")                    
                    $.each(data[0], function (i, val) {
                        // console.log(val);
                        var options = "";
                        $.each(data[1], function (i, data) {
                        	console.log(data);                            
                            options += "<div class=\"col-4\" style=\"margin-bottom: 20px;\">" + data.name + "<input type=\"text\" class=\"form-control\" name=\"Quantities["+ val.id +"][" + data.id + "].Value\" />";                                
                            options += "</div > ";                            
                        });

                        $("#product-create-types").append("<div class=\"option-container row\">"+
                            "<div class=\"col-2\" id=\"product-create-type\"><span>" + val.name + "</span></div>" +
                            "<div class=\"col-10 row\" id=\"product-create-options\"> " + options + " </div></div><hr /> ");                        
                        
                        if(optionType % 100 == 0)
                        {                            
                            $("#product-form").append("<input type=\"hidden\" name=\"OptionRelationId\" value=\"" + optionType + "\">")
                        }                                              
                    });
                    
                },
                error: function (data) {
                    console.log(data);
                }
            })
        })

        $("#product-price").on("keyup", function () {
            if ($(this).val().includes(','))
                $(this).val($(this).val().replace(',', '.'));

            var value = Number($(this).val());
            var vat = {{config('global.VAT')}};

            if (value != 0)
            {
                var newPrice = (value + (value * (vat / 100))).toFixed(2);
                $("#product-price-vat").val(newPrice);
            }
        });

        $("#product-price-vat").on("keyup", function () {
            if ($(this).val().includes(','))
                $(this).val($(this).val().replace(',', '.'));

            var value = Number($(this).val());
            var vat = {{config('global.VAT')}};

            if (value != 0)
            {
                var newPrice = (value - (value * (vat / 100))).toFixed(2);
                $("#product-price").val(newPrice);
            }
        });

    	
        var tempID = '{{$create_session_id}}';        
        var uploadedCounter = 0
        var dz = new Dropzone("div#product-dz", {
            url: "{{route('save_images')}}",
            uploadMultiple: true,
            addRemoveLinks: true,
            parallelUploads: 1,
            init: function () {

                this.on('sending', function (file, xhr, formData) {                	
                    formData.append('create_session_id', tempID);
                });

                this.on('success', function (data, response) {
                    console.log(response);

                });

                this.on('error', function (error, response) {
                    console.log(response);

                });
            },
            sending: function(file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },
            removedfile: function (file) {
                var name = file.name;

                $.ajax({
                    type: 'POST',
                    url: '{{route('delete_image')}}',
                    data: { name: name, id: tempID },
                    success: function (data) {
                        console.log('success: ' + data);
                    },
                    error: function(data){
                    	console.log(data);
                    }
                });
                
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }
        });
    </script>


@endsection