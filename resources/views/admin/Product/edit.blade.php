@extends('admin._layout')

@section ('HeadScripts')
    <script src="{{asset('storage/js/dropzone-5.7.0/dist/dropzone.js')}}"></script>
@endsection

@section('HeadCss')
    <link rel="stylesheet" href="{{asset('storage/js/dropzone-5.7.0/dist/dropzone.css')}}" />
@endsection

@section('content')


<ul class="nav nav-tabs" id="myTab" role="tablist" style="position: relative;">
    <li class="nav-item">
        <a class="nav-link active" id="product-tab" data-toggle="tab" href="#product" role="tab" aria-controls="product" aria-selected="true">Product</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="quantities-tab" data-toggle="tab" href="#quantities" role="tab" aria-controls="quantities" aria-selected="false">Quantities</a>
    </li>
    <li>        
        <input type="button" value="Save" class="btn btn-success top-submit-btn" onclick="$('#product-form').submit()" />       

    </li>
</ul>
<form action="{{ route('product_edit') }}" method="post", id="product-form" enctype = "multipart/form-data">  
	@csrf()
	<div class="tab-content" id="myTabContent" style="padding: 15px 0;">
        <div class="tab-pane fade show active" id="product" role="tabpanel" aria-labelledby="product-tab">
            <h3>Description</h3>

            <div class="form-group">
            	<label for="active">Active: </label>
            	<input type="checkbox" name="active" style= "margin-left: 5px;"
                @if($product->active)
                    checked
                @endif
                
                />                
            </div>
            <div class="form-group">
            	<label for="name">Name: </label>
            	<input type="text" name="name" class="form-control" placeholder="Enter product name" value="{{$product->name}}">
                
            </div>

            <div class="form-group">
            	<label for="category">Category: </label>                
                <select class="form-control" id="product-category" name="category_id">
                    @foreach($categories as $category)
                        @if($category->id == $product->category_id)
                            <option value="{{$category->id}}" selected>{{$category->name}}</option>
                        @else   
                            <option value="{{$category->id}}" >{{$category->name}}</option>
                        @endif
                    @endforeach
                </select>

            </div>

            <div class="form-group">
            	<label for="description"> Description: </label>
                <input type="textarea" name="description" class="form-control" placeholder="Enter description" value="{{$product->description}}" >                
                
            </div>

            <div class="row">
                <!-- <div class="col-sm">
                    <label for="price"> Price: </label>
                    <input type="text" name="price" class="form-control" id="product-price" placeholder="Enter price: ">
                    
                </div> -->
                <div class="col-sm">
                    <label for="pricevat">Price w/VAT ({{config('global.VAT')}}%): </label>
                    <input type="text" name="price_vat" class="form-control" id="product-price-vat" value="{{$product->price_vat}}">                   
                    
                </div>
                <div class="col-sm">
                    <label for="price"> Weight ({{config('global.WEIGHT_UNIT_SHORT')}}): </label>
                    <input type="text" name="weight" class="form-control" id="product-price" placeholder="Enter weight: " value="{{$product->weight}}">
                    
                </div>
            </div>
            <hr>

            <h3>Images</h3>
            <div id="product-dz" class="dropzone dz-clickable">
                <div class="fallback">
                    <input type="file" name="Images" multiple>
                </div>
            </div>
            @if(count($product->images) > 0)            
                
                <h3 style="margin-top: 10px;">
                    Saved Images
                </h3>
                <div class="product-saved-imgs">

                    @foreach($product->images as $image)                    
                        <div class="product-saved-img">
                            <img src="{{asset('storage/icons/x-circle.svg')}}" class="delete" alt="" width="25" height="25" onclick="DeleteImage($(this).parent().remove())">
                            <img src="{{$image->get_image_path()}}" />
                            <input type="hidden" value="{{$image->id}}" name="images[]">
                            <!-- { imageCounter++; } -->
                        </div>
                    @endforeach
                </div>
            @endif



        </div>
        <div class="tab-pane fade" id="quantities" role="tabpanel" aria-labelledby="quantities-tab">
            @if(isset($option_type_names) && count($option_type_names) > 0)            
                <div style="width: 20%;display: flex; margin-bottom: 20px;">
                	<select name="option_types" id="option-type" class="form-control" disabled>
                		@foreach($option_type_names as $name => $id)                		      
                		  <option value="{{$id}}">{{$name}}</option>
                		@endforeach
                	</select>
                    
                    <input type="button" id="optionAdd" class="btn btn-secondary" value="Add" style="margin-left: 10px;" />
                </div>
                <hr />
            @endif

            <!-- Saved product options -->
            <div id="product-create-types">
                @if(isset($saved_options) && count($saved_options) > 0)
                
                    @foreach($saved_options as $option_type)
                    
                        <div class="option-container row">
                            <div class="col-2" id="product-create-type">
                                <span>{{$option_type->name}}</span>
                            </div>
                            <div class="col-10 row" id="product-create-options">
                                @foreach ($option_type->options as $i => $option)
                                
                                    <div class="col-4" style="margin-bottom: 20px;">
                                        {{$option->name}}
                                        <input type="text" class="form-control" value="{{$option->value}}" name="quantities[{{$i}}][value]" /> 
                                        <input type="hidden" name="quantities[{{$i}}][id]" value="{{$option->quantity_id}}">
                                        <input type="hidden" name="quantities[{{$i}}][option_id]" value="{{$option->option_id}}">
                                        <input type="hidden" name="quantities[{{$i}}][option_type_id]" value="{{$option->option_type_id}}">
                                    </div>                                    
                                @endforeach
                            </div>
                        </div>
                        <hr />
                    @endforeach
                @endif

            </div>
        </div>
    </div>

    
    <input type="hidden" name="create_session_id" value="{{$create_session_id}}">
    <input type="hidden" value="{{$product->id}}" name="id" />
    <input type="hidden" value="{{$relation_option_id}}" name="relation_option_id" />
    
</form>

@endsection



@section('BottomScripts')
    <script>

        function DeleteImage(element) {
            $(element).parent().remove()          
        }

        
        // $("#optionAdd").on("click", function () {

        //     var optionType = $("#option-type").val();
        //     var text = $( "#option-type option:selected" ).text();
            
        //     $.ajax({
        //         url: "{{route('add_options_list')}}",
        //         method: "POST",
        //         dataType: "json",
        //         data: { optionTypeId: optionType},
        //         success: function (data) {
        //             console.log(text, data);                    
        //             $("#product-create-types").html("");
        //             $("input[name='OptionRelationId']").remove();
        //             //$("#quantities").empty();
        //             //$("#product-create-type").append("<span>" + text + "</span>")                    
        //             $.each(data[0], function (i, val) {
        //                 // console.log(val);
        //                 var options = "";
        //                 $.each(data[1], function (i, data) {
        //                 	console.log(data);                            
        //                     options += "<div class=\"col-4\" style=\"margin-bottom: 20px;\">" + data.name + "<input type=\"text\" class=\"form-control\" name=\"Quantities["+ val.id +"][" + data.id + "].Value\" />";                                
        //                     options += "</div > ";                            
        //                 });

        //                 $("#product-create-types").append("<div class=\"option-container row\">"+
        //                     "<div class=\"col-2\" id=\"product-create-type\"><span>" + val.name + "</span></div>" +
        //                     "<div class=\"col-10 row\" id=\"product-create-options\"> " + options + " </div></div><hr /> ");                        
                        
        //                 if(optionType % 100 == 0)
        //                 {                            
        //                     $("#product-form").append("<input type=\"hidden\" name=\"OptionRelationId\" value=\"" + optionType + "\">")
        //                 }                                              
        //             });
                    
        //         },
        //         error: function (data) {
        //             console.log(data);
        //         }
        //     })
        // })

        $("#product-price").on("keyup", function () {
            if ($(this).val().includes(','))
                $(this).val($(this).val().replace(',', '.'));

            var value = Number($(this).val());
            // var vat = Model.VAT;

            if (value != 0)
            {
                var newPrice = (value + (value * (vat / 100))).toFixed(2);
                $("#product-price-vat").val(newPrice);
            }
        });

    	
        var id = {{$product->id}};
        var uploadedCounter = 0
        var dz = new Dropzone("div#product-dz", {
            url: "{{route('save_images')}}",
            uploadMultiple: true,
            addRemoveLinks: true,
            parallelUploads: 1,
            init: function () {


                this.on('sending', function (file, xhr, formData) {                	
                    formData.append('product_id', id);
                    formData.append('create_session_id', '{{$create_session_id}}');
                });

                this.on('success', function (data, response) {
                    console.log(response);

                    // var savedImagesCounter = $(".product-saved-img").length;
                    // var idx = savedImagesCounter + uploadedCounter;
                    // $.each(response.images, function (i, dataa) {

                    //     $('#product-form').append("<input type=\"hidden\" value=\"" + dataa.path + "\" name=\"Images[" + idx + "].Path\">")
                    //     $('#product-form').append("<input type=\"hidden\" value=\"" + dataa.name + "\" name=\"Images[" + idx + "].Name\">")
                    //     //$('#product-form').append("<input type=\"hidden\" value=\"" + dataa + "\" name=\"Photos[" + idx + "].IsThumbnail\">")
                    //     uploadedCounter ++;
                    // })

                });

                this.on('error', function (error, response) {
                    console.log(response);

                });
            },
            sending: function(file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },
            removedfile: function (file) {
                var name = file.name;

                $.ajax({
                    type: 'POST',
                    url: '{{route('delete_image')}}',
                    data: { name: name, id: id, session_id: '{{$create_session_id}}' },
                    success: function (data) {
                        console.log('success: ' + data);
                    },
                    error: function(data){
                    	console.log(data);
                    }
                });
                
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }
        });
    </script>
}


@endsection