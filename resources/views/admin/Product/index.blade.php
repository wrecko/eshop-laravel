@extends('admin._layout')

@section('content')


<div class="admin-category-add">
    <h2>Product list</h2>
    <input type="text" placeholder="Search..."  class="form-control mx-sm-3" id="search"/>
    <a href="{{route('product_create')}}" class="btn btn-success">Create</a>
    
</div>


<table class="table table-striped table-dark">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Category name</th>
            <th scope="col">Active</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>   
        
        @foreach($products as $product)
        
        <tr>
            <td>{{$product->id}}</td>
            <td>{{$product->name}}</td>
            <td>{{$product->category->name}}</td>
            <td>{{$product->active}}</td>
            <td>
                
                <a href="{{route('product_edit')}}/{{$product->id}}" class="btn btn-warning">Edit</a>
                <a href="#" onclick="window.confirm('Are you sure?')? window.location.href = '{{route('product_delete', ['id' => $product->id])}}': ''" class="btn btn-danger">Delete</a>
            </td>
        </tr>
        @endforeach

        
        
    </tbody>
</table>
@endsection
