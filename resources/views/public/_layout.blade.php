<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Catastrofy</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <link rel="stylesheet" href="{{asset('storage/css/app.css')}}" />
    <link rel="shortcut icon" href="{{asset('storage/icon.png')}}" type="image/x-icon" />

    @yield("HeadScripts")
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</head>
<body>
    <header>
        <div class="logo">
        </div>

        <nav class="navbar navbar-expand-lg">
            @include('public._homeNav')
        </nav>
    </header>

    <div class="main-container">
        <!-- <partial name="_CookieConsentPartial" /> -->
        @yield('content')
    </div>
    
    <footer class=" footer text-muted">
        <!-- <partial name="Partials/_FooterPartial" /> -->
    </footer>

    <div class="cart-modal modal" tabindex="-1" role="dialog">
    </div>
    
    
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script src="{{asset('storage/js/site.js')}}"></script>
    @yield('BottomScripts')
</body>
</html>


 