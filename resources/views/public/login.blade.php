@extends('public._layout')

@section('content')


<form method="POST" action="login" class="auth-form">    
	@csrf
	<div class="form-horizontal" style="">
	    <div class="form-group">

	    </div>
	    <div class="form-group">
	        <label for="username" class="control-label">Username</label>
	        <div class="">
	            <input type="text" name="username" class="form-control">
	        </div>
	    </div>
	    <div class="form-group">
	        <label for="username" class="control-label">Password</label>
	        <div class="">
	            <input type="password" name="password" class="form-control">
	        </div>
	    </div>
	    <div class="form-group">
	        <button class="btn btn-primary" type="submit">Login</button>
	    </div>
	</div>
</form>


@endsection