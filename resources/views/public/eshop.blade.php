@extends('public._layout')

@section('content')

<div class="eshop-container">
     
    <!-- <div class="eshop-categories list-group">
        foreach (var category in Model.Categories)
        {
            string active = category.Active ? "active" : "";

            <a href="#" class="list-group-item list-group-item-action @active">@category.Name</a>
        }
    </div> -->

    @foreach($categories as $category)
    
        <div class="eshop-category">
            <div class="eshop-category-name">
                <span>{{$category->name}}</span>
            </div>
            <div class="eshop-products">
                @foreach ($category->active_products as $prod)
                
                    <div class="eshop-product">
                        <div class="product-image">
                            <a href="/Eshop/detail/@product.Id">
                                <img src="{{$prod->images[0]->get_image_path()}}" alt="{{$prod->name}}" />
                            </a>
                        </div>
                        <div class="product-name">
                            <a href="{{route('detail', ['id' => $prod->id])}}">
                                {{$prod->name}}
                            </a>
                        </div>
                        <div class="product-desc">
                            {{$prod->description}}
                        </div>
                        <div class="product-price">
                            {{$prod->price_vat}} &euro;
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endforeach

</div>


@endsection












