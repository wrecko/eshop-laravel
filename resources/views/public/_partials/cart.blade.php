
<div class="cart-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="color: black;">
            <div class="modal-header">
                <h5 class="modal-title">Cart</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="cart-products">
                    <thead>
                        <tr>
                            <th style="width: 15%;"></th>
                            <th style="width: 60%;"></th>
                            <th style="width: 15%;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- {int i = 0;} -->
                        @foreach($cart as $id => $product)
                            @foreach($product['options'] as $quantity_id => $option)
                                <tr id="{{$quantity_id}}" product-id="{{$id}}">
                                    <td>
                                        <img src="{{$product['photo']}}" width="50px" />
                                    </td>
                                    <td>
                                        <span class="product-name">{{$product['name']}}</span>
                                        <br />

                                        <span class="product-option">
                                            @if($option['parent_id'] != null)                                            
                                                {{$option['parent_name']}}
                                            @endif
                                            {{$option['child_name']}}

                                        </span>

                                    </td>
                                    <td class="cart-options">
                                        <img src="{{asset('storage/icons/trash-fill.svg')}}" class="cart-product-delete" 
                                        onclick="RemoveFromCart( {{$quantity_id}}, {{$id}} )" />
                                        <div class="cart-adjust-count">
                                            <img src="{{asset('storage/icons/dash.svg')}}" class="cart-remove-count" onclick="AdjustCount(0, $(this))" />
                                            <span>{{$option['quantity']}}</span>
                                            <img src="{{asset('storage/icons/plus.svg')}}" class="cart-add-count" onclick="AdjustCount(1, $(this))" />
                                        </div>

                                        <div class="cart-product-pricesum">
                                            {{$product['prod_price_total']}} &euro;
                                        </div>
                                    </td>

                                </tr>
                           <!--  if (true)
                            {
                                i++;
                            } -->
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
                <hr />
                <div class="cart-pricesum">
                    Spolu:
                    <span>{{$price_total}} &euro;</span>
                </div>
            </div>
            <div class="modal-footer">                
                <a href="{{route('checkout')}}" class="btn btn-primary">Go to Checkout</a>
            </div>
        </div>
    </div>
</div>

