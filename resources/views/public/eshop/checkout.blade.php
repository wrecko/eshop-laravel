
@extends('public._layout')

@section('content')
    <div class="checkout-container">
        @if ($errors->any())
            <div class="alert alert-danger" style="width: 70%;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('checkout') }}" method="post", id="checkout-form">          
            @csrf()
            <div class="checkout-information">
                <h3>Fakturačné a dodacie údaje</h3>
                <hr />

                <div class="form-row w-75 m-b-20">
                    <div class="col">
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name"  value="{{old('name')}}" >                        
                    </div>

                    <div class="col">
                        <input type="text" name="surname" class="form-control @error('surname') is-invalid @enderror" placeholder="Surname" value="{{old('surname')}}" >
                    </div>
                </div>

                <div class="form-row w-75 m-b-20">
                    <div class="col-md-4">
                        <input type="text" name="street" class="form-control @error('street') is-invalid @enderror" placeholder="Street" value="{{old('street')}}" >
                    </div>

                    <div class="col-md-2">
                        <input type="text" name="house_number" class="form-control @error('house_number') is-invalid @enderror" placeholder="House number" value="{{old('house_number')}}" >
                    </div>

                    <div class="col-md-4">
                        <input type="text" name="city" class="form-control @error('city') is-invalid @enderror"  placeholder="City" value="{{old('city')}}" >
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="zip" class="form-control @error('zip') is-invalid @enderror"  placeholder="ZIP" value="{{old('zip')}}">
                    </div>
                </div>
                <div class="form-row w-75 m-b-20">
                    <div class="col">
                        <select name="state" class="form-control" id="state-select">
                            @foreach($states as $state)
                                <option value="{{$state->id}}" phone-prefix="+{{$state->phone_prefix}}">{{$state->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col">
                        <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{old('email')}}" required>
                    </div>
                </div>

                <div class="form-row w-75 m-b-20">
                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="number-prefix"></span>
                            </div>
                            <input type="text" name="phone_number" class="form-control @error('phone_number') is-invalid @enderror"  placeholder="Phone number" value="{{old('phone_number')}}">                            
                        </div>
                        </div>
                    </div>
                <div class="form-row w-75 m-b-20">      
                    <div class="col">
                        <textarea name="note" placeholder="Your note " class="form-control @error('note') is-invalid @enderror" >{{old('note')}}</textarea>
                        
                    </div>
                    
                </div>
                <br />

                <h3>Platba a doprava</h3>
                <hr />
                <span class="mini-head">Vyber platbu:</span>
                <div class="form-row w-75 m-b-20" id="payment">
                    <!-- <label for="none" class="test-label" style="display:none;">
                        <input type="radio" name="payment" value="0" />
                    </label> -->

                    <input type="radio" name="payment" value="0" style="display:none;" />
                    <div class="col">
                        <label for="dobierka" class="test-label  @if(old('payment') == 1)) test-label-active @endif">
                            Dobierka
                            <input type="radio" id="dobierka" name="payment" value="1" @if(old('payment') == 1)) checked @endif/>
                            <img src="{{asset('storage/icons/check.svg')}}" />
                        </label>

                    </div>
                    <div class="col">
                        <label for="ucet" class="test-label @if(old('payment') == 2)) test-label-active @endif">
                            Prevod na účet
                            <input type="radio" id="ucet" name="payment" value="2" @if(old('payment') == 2)) checked @endif/>
                            <img src="{{asset('storage/icons/check.svg')}}" />
                        </label>
                    </div>
                </div>
                <span class="mini-head">Vyber dopravu:</span>
                <div class="form-row w-75" id="transport">
                    <!-- <label for="none" class="test-label" style="display:none;">
                        <input type="radio" name="transport" value="0" />
                    </label> -->

                    <div class="col">
                        <label for="odber" class="test-label @if(old('transport') == 1)) test-label-active @endif">
                            Osobny odber
                            <input type="radio" id="odber" name="transport" value="1" @if(old('transport') == 1)) checked @endif/>
                            <img src="{{asset('storage/icons/check.svg')}}" />
                        </label>
                    </div>

                    <div class="col">
                        <label for="posta" class="test-label @if(old('transport') == 2)) test-label-active @endif">
                            Slovenská pošta
                            <input type="radio" id="posta" name="transport" value="2" @if(old('transport') == 2)) checked @endif/>
                            <img src="{{asset('storage/icons/check.svg')}}" />
                        </label>
                    </div>
                </div>



                <!-- <input type="radio" id="platba-prevodom"  name="payment" value="2" />
                <label for="platba-prevodom">Platba prevodom</label> -->

            </div>

            <div class="checkout-cart-data">
                <table class="cart-products">
                    <thead>
                        <tr>
                            <th style="width: 15%;"></th>
                            <th style="width: 60%;"></th>
                            <th style="width: 15%;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cart as $id => $product)
                            @foreach($product['options'] as $quantity_id => $option)
                            <tr id="{{$quantity_id}}" product-id="{{$id}}">
                                <td>
                                    <img src="{{$product['photo']}}" width="50" />
                                </td>
                                <td>
                                    <span class="product-name">{{$product['name']}}</span>
                                    <br />

                                    <span class="product-option">
                                            @if($option['parent_id'] != null)                                            
                                                {{$option['parent_name']}}
                                            @endif
                                            {{$option['child_name']}}

                                        </span>

                                </td>
                                <td class="cart-options">
                                    <div class="cart-adjust-count">
                                        <span>{{$option['quantity']}} pcs.</span>
                                    </div>

                                    <div class="cart-product-pricesum">
                                        {{$product['price']*$option['quantity']}} &euro;
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
                <div class="cart-pricesum">
                    <span> Spolu: {{$price_total}} &euro;</span>
                </div>
                <div class="checkout-cart-data-action">                    
                    <a href="{{route('eshop')}}" class="btn btn-primary"> Pokračovať v nákupe</a>
                    <a href="#" class="btn btn-success" id="checkout-submit">Odoslať objednávku</a>                    

                </div>
            </div>
        </form>
    </div>

@endsection

@section('BottomScripts')

    <script>

        $( document ).ready(function() {
            var prefix = $("#state-select option:selected").attr('phone-prefix');            
            $("#number-prefix").html(prefix);
        });

        $(".test-label").on("click", function () {
            var parentId = $(this).parent().parent().attr("id");
            $("#" + parentId + " .test-label").removeClass("test-label-active")
            $(this).addClass("test-label-active")
        })

        $("#checkout-submit").click(function () {

            $("#checkout-form").submit()
        })

        $("#state-select").on("change", function () {
            var prefix = $("#state-select option:selected").attr('phone-prefix');            
            $("#number-prefix").html(prefix);            
        });
    </script>
@endsection