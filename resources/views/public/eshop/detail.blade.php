@extends('public._layout')

@section('content')

<div class="eshop-product-detail">

    <div class="product-detail-imgs">        
        
            @foreach($prod->images as $key => $img)     
            	@if($key == 0)
            		<div class="product-detail-imgs-big">
			            <img src="{{$img->path}}" alt="{{$img->name}}" />
			        </div>
			        <div class="product-detail-imgs-list">
            	@endif
                <img src="{{$img->path}}" alt="{{$img->name}}" />
            @endforeach
        </div>
    </div>

    <div class="product-detail-desc">
        <span class="category-name">{{$prod->category->name}}</span>
        <h2>{{$prod->name}}</h2>
        <p>
            {{$prod->description}}
        </p>
        <span class="product-price">{{$prod->price_vat}} &euro;</span>

        <div class="product-options">
        	<form autocomplete="off" style="display: contents;">
	            @if(isset($parent_options) && count($parent_options) > 0)
	                <select class="custom-select mr-sm-2" id="parent-options">
	                    @foreach($parent_options as $parent)                    
	                        <option value="{{$parent->option_parent_id}}">{{$parent->parent_option->name}}</option>
	                    @endforeach
	                </select>
	            @endif

	            <select class="custom-select mr-sm-2" id="options">
	                @foreach($quantities as $quantity)                
	                    <option value="{{$quantity->id}}" option-id="{{$quantity->option_id}}">{{$quantity->option->name}}</option>
	                @endforeach
	            </select>
        	</form>
        </div>

        <div class="add-to-cart">
            <button class="btn btn-success btn-lg" value="" id="addtocart">ADD TO CART</button>
        </div>


    </div>

</div>


<div class="product-detail-similar">

</div>

@endsection

@section('BottomScripts')


    <script>    	
        $("#parent-options").on("change", function () {            
            $.ajax({
                url: "{{route('get_options')}}",
                method: "POST",
                dataType: "json",
                data: {parent_id: $(this).val(), product_id: '{{$prod->id}}'},
                success: function (data) {
                    // console.log(data);
                    $("#options").html("")
                    $.each(data.options, function (name, id) {
                        $("#options").append("<option value=\"" + id + "\">" + name + "</option>")
                    })

                    
                },
                error: function (data) {
                    console.log(data);
                }
            });
        })

        $("#addtocart").on("click", function () {
            var parentId = $("#parent-options").val();
            var quantityId = $("#options").val();
            var optionId = $("#options option:selected").attr("option-id");

            $.ajax({
                url: "{{route('add_to_cart')}}",
                method: "POST",
                dataType: "json",
                data: { id: '{{$prod->id}}', quantity_id: quantityId, option_id: optionId, parent_id: parentId },
                success: function (data) {
                    console.log(data);
                    $("#product-count").html(data.count)
                    
                    
                },
                error: function (data) {
                    console.log(data);
                }
            });
        })
    </script>

@endsection