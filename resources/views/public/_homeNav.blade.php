


<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="/eshop">Eshop</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="#">Koncerty</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="#">Media</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="#">Kontakt</a>
        </li>
    </ul>

</div>

<div class="cart">
    <span id="product-count">{{$prodCount}}</span>
    <img src="{{ asset('storage/cart.png') }}" />
</div>
