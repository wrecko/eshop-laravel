<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\Authenticate;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProductOptionController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ImageStoreController;
use App\Http\Controllers\EshopController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\SettingsController;


Route::get('/', [HomeController::class, 'Index'])->name('home');

Route::get('login', [AuthController::class, 'Login'])->name('login');
Route::get('logout', [AuthController::class, 'Logout']);
Route::post('login', [AuthController::class, 'LoginPost']);

Route::prefix('eshop')->group(function(){
	Route::get('/', [EshopController::class, 'index'])->name('eshop');
	Route::get('detail/{id}', [EshopController::class, 'detail'])->name('detail');
	Route::post('get_options', [EshopController::class, 'get_options_for_parent'])->name('get_options');
	Route::get('checkout', [EshopController::class, 'checkout'])->name('checkout');
	Route::post('checkout', [EshopController::class, 'checkout_post'])->name('checkout');

	Route::prefix('cart')->group(function()
	{
		Route::post('show', [CartController::class, 'cart'])->name('show_cart');
		Route::post('add_to_cart', [CartController::class, 'add_to_cart'])->name('add_to_cart');
		Route::post('remove_from_cart', [CartController::class, 'remove_from_cart'])->name('remove_from_cart');
		Route::post('adjust_count', [CartController::class, 'adjust_cart_product_count'])->name('adjust_count');
	});
});





Route::prefix('test')->group(function(){
	Route::get('cart',  function(){
		 return view('public._partials.cart', ['cart' => session()->get('cart')]);
	});
	Route::get('test', function(){
		$cart = session()->get('cart');
		var_dump(\App\Lib\Cart::getProductCount()  );
		var_dump($cart);
		var_dump($cart['products'][1]['options']);
		$cart = [];
		// session()->put('cart', $cart);
	});

	Route::get('mail', function(){

		$to_name = 'Tomas';
		$to_email = 'r.vrecnik@gmail.com';
		$data = array('name'=>"Ogbonna Vitalis", "body" => "A test mail");
		// Mail::send("emails.mail", $data, function($message) use ($to_name, $to_email) {$message->to($to_email, $to_name)->subject('Laravel Test Mail');$message->from('wrecko@centrum.sk','Test Mail');
		// });

		Mail::to($to_email)->send(new App\Mail\OrderCreated(new App\Models\Order()));
	});

			

	
});



Route::middleware([Authenticate::class])->group(function (){

	Route::get('admin', [AdminController::class, 'Index']);
	Route::prefix('admin')->group(function()
	{
		Route::get('product_option', [ProductOptionController::class, 'ProductOption'])->name('options');

		Route::post('optionCreate', [ProductOptionController::class, 'OptionCreate'])->name('option_create');	
		Route::post('optionUpdate', [ProductOptionController::class, 'OptionUpdate'])->name('option_update');
		Route::get('optionDelete/{id}', [ProductOptionController::class, 'POptionDelete'])->name('option_delete');

		Route::post('optionTypeCreate', [ProductOptionController::class, 'OptionTypeCreate'])->name('option_type_create');	
		Route::post('optionTypeUpdate', [ProductOptionController::class, 'OptionTypeUpdate'])->name('option_type_update');	
		Route::get('optionTypeDelete/{id}', [ProductOptionController::class, 'OptionTypeDelete'])->name('option_type_delete');

		Route::post('optionRelationCreate', [ProductOptionController::class, 'OptionRelationCreate'])->name('option_relation_create');	
		Route::post('optionRelationUpdate', [ProductOptionController::class, 'OptionRelationUpdate'])->name('option_relation_update');	
		Route::get('optionRelationDelete/{id}', [ProductOptionController::class, 'OptionRelationDelete'])->name('option_relation_delete');	

		Route::get('category', [ProductCategoryController::class, 'index'])->name('categories');
		Route::post('categoryCreate', [ProductCategoryController::class, 'create'])->name('category_create');
		Route::post('categoryUpdate', [ProductCategoryController::class, 'update'])->name('category_update');
		Route::get('categoryDelete/{id}', [ProductCategoryController::class, 'delete'])->name('category_delete');

		Route::get('product', [ProductController::class, 'index'])->name('products');		
		Route::prefix('product')->group(function(){
				Route::get('create', [ProductController::class, 'create'])->name('product_create');
				Route::post('create', [ProductController::class, 'createpost'])->name('product_create');	
				Route::post('save_images', [ImageStoreController::class, 'storeimages'])->name('save_images');		
				Route::post('delete_images', [ImageStoreController::class, 'deleteimages'])->name('delete_image');	
				Route::post('add_options_list', [ProductController::class, 'addoptionslist'])->name('add_options_list');	


				Route::get('edit/{id}', [ProductController::class, 'update'])->name('product_edit');
				Route::post('edit', [ProductController::class, 'updatepost'])->name('product_edit');
				Route::get('delete/{id}', [ProductController::class, 'delete'])->name('product_delete');
		});

		Route::prefix('order')->group(function(){
			Route::get('/', [OrderController::class, 'index'])->name('orders');
			Route::get('/{id}', [OrderController::class, 'detail'])->name('order_detail');
			//	TODO maybe change to post???
			Route::get('change_status/{id}/{status}', [OrderController::class, 'change_status'])->name('change_status');
		});

		Route::get('settings', [SettingsController::class, 'index'])->name('settings');	
		Route::prefix('settings')->group(function(){
			Route::post('save_fees', [SettingsController::class, 'save_fees'])->name('save_fees');	

		});
		
	});
});




