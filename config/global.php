<?php

return [

    'VAT' 						=> 20,
    'IMAGE_UPLOAD_PATH' 		=> 'images/product/',
    'WEIGHT_UNIT' 				=> 'gram',
    'WEIGHT_UNIT_SHORT'			=> 'g',
]

?>